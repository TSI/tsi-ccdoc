#!/usr/bin/env bash

kubectl version
kubectl config get-contexts
kubectl cluster-info
kubectl get node
