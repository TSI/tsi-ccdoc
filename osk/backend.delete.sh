#!/usr/bin/env bash

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

kubectl delete -f ${DIR}/minio.yml
kubectl delete -f ${DIR}/mysql.yml

#kubectl get event --sort-by='{.lastTimestamp}'
kubectl get svc
kubectl get ing
kubectl get pod
