#!/usr/bin/env bash

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

mysqlpass=$1
accesskey=$2
secretkey=$3

kubectl create secret generic mysql-pass --from-literal=password=${mysqlpass}
kubectl create secret generic minio-keys --from-literal=accesskey=${accesskey} --from-literal=secretkey=${secretkey}
kubectl get secret

kubectl apply -f ${DIR}/minio.yml
kubectl apply -f ${DIR}/mysql.yml

kubectl rollout status statefulset.apps/minio-sts --timeout=60m
kubectl rollout status statefulset.apps/mysql-sts --timeout=60m

#kubectl get event --sort-by='{.lastTimestamp}'
kubectl get pvc
kubectl get svc
kubectl get ing
kubectl get pod