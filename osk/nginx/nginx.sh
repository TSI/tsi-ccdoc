#!/usr/bin/env bash

# DIR where the current script resides
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

kubectl apply -f ${DIR}/nginx.yml
kubectl rollout status deployment.v1.apps/nginx-deploy --request-timeout=60m

#kubectl get event --sort-by='{.lastTimestamp}'
kubectl get svc
kubectl get ing
kubectl get pod