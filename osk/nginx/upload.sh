#!/bin/bash

# A simple wraper for kubectl cp so that the parent directory name is not included in the upload
# Usage: ./upload ${sourcedir} ${targetdir} ${podname}

sourcedir=$1
targetdir=$2

podname=$(kubectl get pods -o=jsonpath='{.items[?(@.spec.containers[*].name=="nginx")].metadata.name}' | cut -d' ' -f1)

files=( $(ls ${sourcedir}) )
for file in ${files[@]}; do
  echo ${sourcedir}/${file}
  kubectl cp ${sourcedir}/${file} ${podname}:${targetdir}
done
