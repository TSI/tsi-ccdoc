ResOps Training & Workshop - 2019
=================================

Introduction
------------
ResOps training is mainly focus to research community who are willing to develop their cloud native skills. The ResOps course will not be delivered in-person for the foreseeable future, This course can be taken standalone, self-paced and for **FREE**.

Course objectives
-----------------

This workshop will provide some background to cloud computing and practical experience in building, deploying and running applications in cloud platforms - OpenStack, Google, Amazon and Azure.

Using examples drawn from EMBL-EBI’s experiences in the life-sciences, but using tools and technologies that are cross-platform and cross-domain, attendees will come away with a knowledge as to the user-centric hybrid cloud strategy as well as practical aspects of deploying across different clouds and the architectural considerations around porting applications to a cloud.

Prerequisite
------------

* Installation of Minikube and GIT, on your own laptop OR your VM* where you want to do your hand-on exercise.
* If you are choosing VM as your choice then please use at least 1vCPU, RAM 4GB and storage as per your choice.
* Understanding of Linux commands and GIT repository commands
* An account should be created on `Public GitLab <https://gitlab.com/users/sign_in>`_ CI/CD practical with GitLab.
* We are providing sandbox over OpenStack cloud to play exercises.
* Workshop participants can use Nano editor by default. Other CLI editors are also available in the sandbox.

Course guidelines
------------------

+----------+------------------------------------------------------------------------------------------------------------------+
| Duration | Topic                                                                                                            |
+==========+==================================================================================================================+
|  30 min  | `Cloud 101 <../../_static/pdf/resops2019/Cloud-101.pdf>`_                                                        |
+----------+------------------------------------------------------------------------------------------------------------------+
|  10 min  | `EBI Cloud Services <../../_static/pdf/resops2019/EBI_Cloud_Services_v1.0.pdf>`_                                 |
+----------+------------------------------------------------------------------------------------------------------------------+
|  40 min  | `Porting apps into clouds <../../_static/pdf/resops2019/PortingResearchPipelinesintoClouds.pdf>`_                |
+----------+------------------------------------------------------------------------------------------------------------------+
|  60 min  | `Creating Containers with Docker <../../_static/pdf/resops2019/Creating-Containers-with-Docker.pdf>`_            |
+----------+------------------------------------------------------------------------------------------------------------------+
|  90 min  | `Docker Practicals <Docker.html>`_                                                                               |
+----------+------------------------------------------------------------------------------------------------------------------+
|  60 min  | `Introduction to Gitlab <../../_static/pdf/resops2019/Introduction-to-Gitlab.pdf>`_                              |
+----------+------------------------------------------------------------------------------------------------------------------+
|  90 min  | `GitLab Practicals <Gitlab.html>`_                                                                               |
+----------+------------------------------------------------------------------------------------------------------------------+
|  60 min  | `Kubernetes 101 <../../_static/pdf/resops2019/Kubernetes-101.pdf>`_                                              |
+----------+------------------------------------------------------------------------------------------------------------------+
|  120 min | `Overview <../../_static/pdf/resops2019/KubernetesPracticals.pdf>`_                                              |
|          | / `Kubernetes (Demo) <Kubernetes-Demo-2019.html>`_                                                               |
+----------+------------------------------------------------------------------------------------------------------------------+
|  45 min  | `Overview <../../_static/pdf/resops2019/KubernetesPracticals.pdf>`_                                              |
|          | / `Kubernetes Practical <Minikube-and-NGINX-Practical-2019.html>`_                                               |
+----------+------------------------------------------------------------------------------------------------------------------+
|  180 min | `Overview <../../_static/pdf/resops2019/KubernetesPracticals.pdf>`_                                              |
|          | / `Advanced Kubernetes Practical <Scaling-up-Kubernetes.html>`_                                                  |
+----------+------------------------------------------------------------------------------------------------------------------+
| 150 min  | `Overview <../../_static/pdf/resops2019/KubernetesPracticals.pdf>`_                                              |
|          | / `Advanced Kubernetes Reading <Important-considerations-for-research-pipelines.html>`_                          |
+----------+------------------------------------------------------------------------------------------------------------------+

.. footer:: These resources are being developed as part of the EOSC-Life project. EOSC-Life has received funding from the European Union’s Horizon 2020 programme under grant agreement number 824087.

