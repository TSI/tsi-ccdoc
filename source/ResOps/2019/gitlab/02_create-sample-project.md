## Exercise 2: Create a new project ##

### Objective ###

Create a new project and import the sample code, see the build process it triggers.

### Create the project and upload the code, watch the build fail ###

- In the browser, click on **Projects** -> **Your projects**, from the top-left of the window.
- Click 'New Project', on the top-right
  - if you don't have any projects yet, there will be a big 'Create a project' button on the left instead
- Give the project a name, e.g. 'gitlab-101' or 'my-test-project'
  - add a description if you like, e.g. a reference to this tutorial
  - set the project visibility to 'public'
  - **do not* click the 'Initialize repository with README' button
- Click 'Create Project'
  - that's all you need to do in this browser window, you don't need to follow the instructions you see on-screen next, instead do what it says below.

Create an empty directory, go into it, and create an empty git repository there:

```
mkdir ~/gitlab-test
cd ~/gitlab-test
git init
```

Add the code to your repository and commit it:

```
tar xvf ~/tsi-ccdoc/tsi-cc/ResOps/scripts/gitlab/tiny-test.tar

# replace gitlab.com with gitlab.ebi.ac.uk if you're using the EBI gitlab
git remote add origin https://gitlab.com/YOUR_GITLAB_USERNAME/YOUR_PROJECT_NAME.git

git add .
git commit -m "Initial commit"
git push -u origin master
```

You will be prompted for your usename and password.

[if project shows as empty then go to Projects -> Your Projects -> click on your project. Then Settings -> General -> Advanced Settings -> Change path. Change it, then change it back again]: #

Then go to **Projects** -> **Your Projects** and click on your project. Go to the **CI/CD** -> **Pipelines** tab and watch the progress of your pipeline. Unfortunately it will fail!

### Fix the build and try again ###

The build fails because the code which is uploaded points to my personal project, which only I have access to.

To fix this, edit the **.gitlab-ci.yml** file, change the **APPLICATION** name to the name of your project, and the **REGISTRY_USER** to your gitlab username. Save the file, commit it to git, and push it to the server again. This time, you only need a ```git push```, with no extra arguments.

Go to the **CI/CD** -> **Pipelines** tab again, this time it will succeed.

### Conclusion ###

You now know how to create a project in gitlab, how to upload code to it from an existing area on your computer, and how to configure the basic features of the CI/CD pipeline to refer to this project correctly.

### Best Practices ###

Keep your projects small, it's better to have many small projects than a few large ones with everything bundled in together. It keeps the CI/CD pipeline cleaner and faster, as well as being good coding hygiene in general.

If you have lots of projects, shared between many developers, you can look into using gitlab **Groups** to organise them.