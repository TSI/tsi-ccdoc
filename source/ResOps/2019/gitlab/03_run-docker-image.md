## Exercise 3: Download and run the docker image from your gitlab project ##

### Objective ###

Learn how to download and run docker images built from your CI/CD pipelines.

### Introduction ###

If your pipeline ran to completion in the previous exercise, you can now go to the **Packages** tab on your project page, then to **Container Registry**.

If you're on gitlab.ebi.ac.uk, the interface is different (as of 2019-07-07), and you can just go to the **Registry** tab on the project page directly.

Look near the top-left of the page, where you will see a link with your gitlab username and the name of the project in it. Click on that, and you'll see the list of images you've built so far. There will be only one image, tagged **latest**, with a size of about 45 MB:

![](/static/images/resops2019/gitlab-03-docker-registry.png)

How you run the image depends on wether or not you made your project public. If your project is private, you'll need to log into the docker registry first, giving your username and Personal Access Token when prompted:

```docker login registry.gitlab.com```

You only need to login once per machine, the credentials are cached.

If you're doing the exercises on the EBI gitlab instance, use **dockerhub.ebi.ac.uk** instead of **registry.gitlab.com**

Then simply run the image - note that you'll have to specify your own username and the name of your project, of course:

```
> docker run registry.gitlab.com/tonywildish/tiny-test
Unable to find image 'registry.gitlab.com/tonywildish/tiny-test:latest' locally
latest: Pulling from tonywildish/tiny-test
7413c47ba209: Pull complete 
0fe7e7cbb2e8: Pull complete 
1d425c982345: Pull complete 
344da5c95cec: Pull complete 
7e8d808bd962: Pull complete 
1b5e91f7df4f: Pull complete 
Digest: sha256:a1fc0a6209ac87cc8a009e8e2bee2cbfb528f246e2fd174d15a741efe7433bf6
Status: Downloaded newer image for registry.gitlab.com/tonywildish/tiny-test:latest
Hello World
Compiled on Tue Jul 30 09:54:56 UTC 2019
```

Docker supports multiple levels of namespacing within a repository, so it's possible to build more than one image from a given code base if you want to. Check the instructions on the Registry page for how to do that. This can be useful if you need it, but you should always consider if it's better to have multiple separate projects instead of a single project that's building many containers.

Now that you're logged into the registry you can even push images there by hand, just like on dockerhub.com. That, of course, rather defeats the point of having an automatic CI/CD pipeline to do it for you!

### Deploy tokens ###

If you want to run this docker image in a batch script, or a web service, you won't want to have to log into the registry on every machine you use, or every time it gets rebooted. Instead, if you only want to _use_ the images, but not to _upload_ them from the command line, you want a way to have read-only access to the registry. You can make the registry public by making the project public, which works just fine, but there may be reasons you don't want to do that.

Gitlab provides **deploy-tokens** to address this problem. Deploy tokens are basically the same as the personal access token you've already created in the first exercise. For running batch jobs using containers from a private project registry, you should create a deploy token, giving it only **read_registry** access, nothing else. You can then safely publish this anywhere you like, the worst people can do with it is to download your images.

To create a deploy token, go to the dashboard for your project (deploy tokens are project-specific, unlike personal access tokens). Then go to **Settings** -> **Repository** and scroll down until you find **Deploy Tokens**. You can allow access to just the registry, just the code repository, or both, as you see fit.

You'll need to make a note of the expiry date of your deploy tokens, and set a reminder to create a new one well in advance if you're running in a production environment. Alternatively, if your containers are fit for public release, you can push them to a public service like dockerhub.com, either as well as or instead of using the gitlab registry.

### Conclusion ###

You now know how to inspect the docker registry associated with your gitlab account, how to log into it from the command line, and how to download and run images from it. By using a deploy-token, you can now use even private docker images in your pipelines.

### Best Practices ###

- Use the namespace hierarchy if you need to build multiple docker images from the same code base, use separate projects if the code bases are not related.
- Use deploy tokens when you only need read-only access to private images.