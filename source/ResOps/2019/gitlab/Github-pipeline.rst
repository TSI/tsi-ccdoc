Gitlab to Github pipeline
-------------------------

.. contents::

Code Pipeline
~~~~~~~~~~~~~

There are teams/projects that need their public facing code to be on Github and their in-house code to be in Gitlab.
The Gitlab to Github automated code pull/push can be set up and found at,

https://docs.gitlab.com/ee/user/project/import/github.html


Release Automation
~~~~~~~~~~~~~~~~~~

There are cli tools available to automate release of code from gitlab pipelines to any gitlab or github project by 
the use of auth token.

Tools used:

- Github - https://github.com/github/hub
- Gitlab - https://pypi.org/project/gitlab-release/

Implementation
~~~~~~~~~~~~~~

The below .gitlab-ci.yml demonstrates release automation of binary files generated on 'dist/' artifact from build(not shown) stage.
This implementation has been taken from https://gitlab.ebi.ac.uk/TSI/RDSDS/DSDS-Client/blob/master/.gitlab-ci.yml.
 

.. code:: yaml
	
	release-gitlab:
	  stage: release
	  image: python
	  script:
	    - pip3 install gitlab-release
	    - cd dist/
	    - gitlab-release --server ${GITLAB_SERVER}/${IMAGE_GROUP}/${IMAGE_PROJECT} --project_id ${IMAGE_PROJECT_ID} --description ${CI_REGISTRY}/${IMAGE_GROUP}/${IMAGE_PROJECT}/${IMAGE_NAME}:${CI_BUILD_TAG} dsds-client
	  artifacts:
	    paths:
	      # Must include files passed to gitlab_release
	      - dist/
	  only:
	    - tags
    
	release-github:
	  stage: release
	  image: debian
	  script:
	    - apt-get update && apt-get install -y hub gettext-base
	    - mkdir -p /root/.config
	    - envsubst < hub.template > /root/.config/hub
	    - hub clone --bare https://${GITHUB_USER}:${GITHUB_TOKEN}@github.com/${GITHUB_REPO}.git cloned-project/
	    - mkdir -p cloned-project/dist
	    - cp -r dist cloned-project/
	    - cd cloned-project
	    - '[ -z "${RELEASE_MESSAGE}" ] && export RELEASE_MESSAGE="${CI_COMMIT_REF_NAME}\n\nRelease-${CI_COMMIT_REF_NAME}"|| echo "Release message:\n${RELEASE_MESSAGE}"'
	    - echo -e ${RELEASE_MESSAGE} > release_mssg.txt
	    - cat release_mssg.txt
	    - 'hub release create -F release_mssg.txt -a "dist/dsds-client#dsds-client-centos" ${CI_COMMIT_REF_NAME}'
	  artifacts:
	    paths:
	      # Must include files passed to gitlab_release
	      - dist/
	  only:
	    - tags


