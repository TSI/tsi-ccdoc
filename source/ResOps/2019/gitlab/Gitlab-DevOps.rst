Gitlab DevOps
===============

This document is based on Gitlab instance deployed in EBI by web production team. The reference of the instance can be found `here <https://www.ebi.ac.uk/seqdb/confluence/x/lI1OB>`_.

.. contents::


Pre-requisites
--------------
- Should have EBI Gitlab account
- Basic knowledge on Docker/Kubernetes

DevOps - Sounds familiar?
-------------------------
**DevOps** can be best explained as people working together to build, deliver, 
and run resilient software at the speed of their particular business. DevOps 
enables software development (**Dev**) and operations (**Ops**) teams to accelerate 
delivery through automation, collaboration, fast feedback, and iterative improvement.

For more, check https://about.gitlab.com/devops/

Why Gitlab for DevOps?
----------------------
There are a lot of DevOps tools out there. As a single application for the entire DevOps 
life cycle, GitLab can remove the pain of having to choose, integrate, learn, and maintain 
the multitude of tools necessary for a successful DevOps tool chain.

For more, check https://about.gitlab.com/devops-tools/

Auto DevOps
-----------
Auto DevOps provides pre-defined CI/CD configuration which allows you to automatically detect, 
build, test, deploy, and monitor your applications. Leveraging CI/CD best practices and tools, 
Auto DevOps aims to simplify the setup and execution of a mature & modern software development 
lifecycle.

Pros
~~~~~
With Auto DevOps, the software development process becomes easier to set up as every project can 
have a complete workflow from verification to monitoring with minimal configuration. Just push your 
code and GitLab takes care of everything else. This makes it easier to start new projects and brings 
consistency to how applications are set up throughout a company.

Cons
~~~~~
As you can expect, it is not possible for Gitlab to detect and setup deployment for each and every possible 
application. There are certain expectation from application that Gitlab required it to be adhered to for Auto-DevOps
to be working without any manual scripting, details on this later. Further, having Gitlab doing all the build and deployment 
might not be suitable use-case for everyone.

Pre-requisites
~~~~~~~~~~~~~~
- GitLab Runner
- Base domain
- Kubernetes
- Prometheus

For more, https://docs.gitlab.com/ee/topics/autodevops/#requirements

Expectation
~~~~~~~~~~~
Although it is called *Auto* DevOps, there are some assumptions have been made for Auto Deployment to be working for the application.
The assumptions(and experiences!) are as follows,

- Application should be listening on 5000 port
- Better to write your own Dockerfile, things generally don't work well with hiroku build packs

You can customize some of the parts, more details on https://docs.gitlab.com/ee/topics/autodevops/#customizing

Example
~~~~~~~~
You can find the implementation of Auto-DevOps in,

https://gitlab.ebi.ac.uk/soumyadip/minimal-ruby-app

and sample pipeline in the same project,

https://gitlab.ebi.ac.uk/soumyadip/minimal-ruby-app/pipelines/22970

Security Dashboard, containing test result after running Auto-DevOps tests,

https://gitlab.ebi.ac.uk/soumyadip/minimal-ruby-app/security/dashboard/?project_id=1190&page=1&days=90

Metrics of the running service can be found at,

https://gitlab.ebi.ac.uk/soumyadip/minimal-ruby-app/environments/115/metrics
