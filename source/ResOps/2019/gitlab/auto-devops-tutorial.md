## Auto DevOps tutorial ##

### Objective ###
Learn how to setup Auto-DevOps. 

### Forking Repo ###
Fork the Auto-DevOps example provided by gitlab from below,

https://gitlab.com/auto-devops-examples/minimal-ruby-app

### Adding Kubernetes Cluster ###
You can create cluster from **Operations** > **Kubernetes** > **Add Kubernetes cluster** > **Create cluster on GKE**

You need to create account on Google Cloud in able to use this feature. Once the cluster is created, you need to enable,

- **Ingress** (For load balancing)
- **cert-manager** (Optional - for SSL certificates)
- **Prometheus** (Optional - for monitoring cluster/application)

Once Ingress setup is done you need to set base domain as $public-ip$.nip.io. There should be suggestion below base domain input box for your reference.

### Enabling Auto-DevOps ###
Enable Auto-DevOps from **Settings** > **CI/CD** > **Auto-DevOps**,

![](/static/images/resops2019/auto-devops-capture-1.png)

### Checking Deployment ###
Once Auto-Devops is enabled, you can go to **CI/CD** to check for latest pipeline. Latest pipeline should show application build and deployment steps.
Your application link should be displayed in **Production** stage.

### Checking Security Dashoard ###
Once the pipeline is succeded, you can go to **Security & Complaince** > **Security Dashboard** to check for security scan reports.

### Checking Monitoring Dashoard ###
If you have enabled **Prometheus** in Cluster Applications, you can check **Operations** > **Metrics** for application CPU/Memory/IO metrics.

### Conclusion ###
This tutorial guides you through setting up auto-devops with a sample application. But there are some customization needed for your application to be enabled for auto-devops. You can find more about customization in,

https://docs.gitlab.com/ee/topics/autodevops/customize.html

### Best Practices ###
1. Always have Dockerfile ready, don't let auto-devops build using build packs. Generally it does not result well.
2. Instead of enabling auto-devops through settings, you can create .gitlab-ci.yml and import auto-devops template. It does basically the same thing but the later gives you more flexibility in case you need to add more steps.
3. If you are trying out in cloud kubernetes services, be mindful this can incur heavy cost. Please destroy the cluster once you have tried out.
4. Adding your own cluster is the cost-efficient option. If your organization provides kubernetes cluster in private cloud, you can add the cluster following below guide,
https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html
