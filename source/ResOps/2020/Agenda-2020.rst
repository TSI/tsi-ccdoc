ResOps Training & Workshop - 2020
=================================

Introduction
------------
ResOps training is mainly focus to research community who are willing to develop their cloud native skills. The ResOps course will not be delivered in-person for the foreseeable future, This course can be taken standalone, self-paced and for **FREE**.

Course objectives
-----------------

This workshop will provide some background to cloud computing and practical experience in building, deploying and running applications in cloud platforms - OpenStack, Google, Amazon and Azure.

Using examples drawn from EMBL-EBI’s experiences in the life-sciences, but using tools and technologies that are cross-platform and cross-domain, attendees will come away with a knowledge as to the user-centric hybrid cloud strategy as well as practical aspects of deploying across different clouds and the architectural considerations around porting applications to a cloud.

Prerequisite
------------

* An account should be created on `Public GitLab <https://gitlab.com/users/sign_in>`_ CI/CD practical with GitLab.
* We are providing sandbox over OpenStack cloud to play exercises.
* Workshop participants can use Nano editor by default. Other CLI editors are also available in the sandbox.

Course videos
-------------

The course videos are all `available for download <http://bit.ly/resops-videos>`_ for self-paced study. The slides are linked to the talks in the agenda.

Course guidelines
------------------

+------------------------+----------+----------------------------------------------------------------------------------+
| Date & Time (CEST)     | Duration | Topic                                                                            |
+========================+==========+==================================================================================+
| Monday 05 Oct          |          |                                                                                  |
+------------------------+----------+----------------------------------------------------------------------------------+
|         14:00 - 14:10  | 10 min   | `Introduction  <../../_static/pdf/resops2020/Introduction.pdf>`_                 |
+------------------------+----------+----------------------------------------------------------------------------------+
|         14:10 - 14:40  | 30 min   | `Cloud 101 <../../_static/pdf/resops2020/Cloud-101.pdf>`_                        |
+------------------------+----------+----------------------------------------------------------------------------------+
|         14:40 - 14:50  | 10 min   | `EBI Cloud Services`_  (for courses at EBI only)                                 |
+------------------------+----------+----------------------------------------------------------------------------------+
|         14:40 - 15:20  | 40 min   | Porting apps into cloud [ `Part 1`_ | `Part 2`_ | `Part 3`_ ]                    |
+------------------------+----------+----------------------------------------------------------------------------------+
|         15:20 - 16:20  |  60 min  | `Creating Containers with Docker`_                                               |
+------------------------+----------+----------------------------------------------------------------------------------+
|         16:20 - 16:30  |  10 min  | Break                                                                            |
+------------------------+----------+----------------------------------------------------------------------------------+
|         16:30 - 18:00  |  90 min  | `Docker Practicals`_                                                             |
+------------------------+----------+----------------------------------------------------------------------------------+
| Tuesday 06 Oct         |          |                                                                                  |
+------------------------+----------+----------------------------------------------------------------------------------+
|         14:00 - 15:00  |  60 min  | `Introduction to Gitlab`_                                                        |
+------------------------+----------+----------------------------------------------------------------------------------+
|         15:00 - 16:30  |  90 min  | `GitLab Practicals`_                                                             |
+------------------------+----------+----------------------------------------------------------------------------------+
|         16:30 - 16:45  |  15 min  | `GitLab Auto DevOps`_                                                            |
+------------------------+----------+----------------------------------------------------------------------------------+
|         16:45 - 17:05  |  20 min  | `Demo - GitLab Auto DevOps`_                                                     |
+------------------------+----------+----------------------------------------------------------------------------------+
|         17:05 - 18:00  |  55 min  | Q&A, continue with practicals                                                    |
+------------------------+----------+----------------------------------------------------------------------------------+
| Thursday 08 Oct        |          |                                                                                  |
+------------------------+----------+----------------------------------------------------------------------------------+
|         14:00 - 15:00  |  60 min  | `Kubernetes 101`_                                                                |
+------------------------+----------+----------------------------------------------------------------------------------+
|         15:00 - 17:00  | 05 min   | `Overview of Kubernetes (Demo)`_                                                 |
+------------------------+----------+----------------------------------------------------------------------------------+
|                        | 115 min  | `Kubernetes (Demo)`_                                                             |
+------------------------+----------+----------------------------------------------------------------------------------+
|         17:00 - 17:45  | 05 min   | `Overview of K8S Practical`_                                                     |
+------------------------+----------+----------------------------------------------------------------------------------+
|                        | 40 min   | `Kubernetes Practical`_                                                          |
+------------------------+----------+----------------------------------------------------------------------------------+
|         17:45 - 17:50  | 05 min   | `Overview of Advanced K8S Practical`_                                            |
+------------------------+----------+----------------------------------------------------------------------------------+
|         17:50 - 17:55  | 05 min   | `Summary`_                                                                       |
+------------------------+----------+----------------------------------------------------------------------------------+
|                        |          |                                                                                  |
+------------------------+----------+----------------------------------------------------------------------------------+
|                        |          |  **Advanced Kubernetes**                                                         |
+------------------------+----------+----------------------------------------------------------------------------------+
|                        |          | `Deployment and Deployment Strategies`_                                          |
+------------------------+----------+----------------------------------------------------------------------------------+
|                        |          | `Service Mesh`_                                                                  |
+------------------------+----------+----------------------------------------------------------------------------------+
|                        |          | `Advanced Kubernetes Practical`_ [Homework]                                      |
+------------------------+----------+----------------------------------------------------------------------------------+
|                        |          | `Advanced Kubernetes Reading`_ [Homework]                                        |
+------------------------+----------+----------------------------------------------------------------------------------+

.. _Cloud 101: ../../_static/pdf/resops2020/Cloud-101.pdf
.. _EBI Cloud Services: ../../_static/pdf/resops2020/EBI_Cloud_Services_v1.0.pdf
.. _Part 1: ../../_static/pdf/resops2020/PortingResearchPipelinesintoClouds1.pdf
.. _Part 2: ../../_static/pdf/resops2020/PortingResearchPipelinesintoClouds2.pdf
.. _Part 3: ../../_static/pdf/resops2020/PortingResearchPipelinesintoClouds3.pdf
.. _Creating Containers with Docker: ../../_static/pdf/resops2020/Creating-Containers-with-Docker.pdf
.. _Introduction to Gitlab: ../../_static/pdf/resops2020/Introduction-to-Gitlab.pdf
.. _GitLab Auto DevOps: ../../_static/pdf/resops2020/Gitlab_Auto-DevOps.pdf
.. _Demo - GitLab Auto DevOps: https://drive.google.com/file/d/1atLANuG8Com64I0TBdcXUER8IlgucE_O/view?usp=sharing
.. _Kubernetes 101: ../../_static/pdf/resops2020/Kubernetes-101.pdf
.. _Overview of Kubernetes (Demo): ../../_static/pdf/resops2020/KubernetesPracticalsDemo.pdf
.. _Overview of K8S Practical: ../../_static/pdf/resops2020/KubernetesPracticals.pdf
.. _Overview of Advanced K8S Practical: ../../_static/pdf/resops2020/KubernetesAdvancedPracticals.pdf
.. _Summary: ../../_static/pdf/resops2020/Summary.pdf
.. _Docker Practicals: ../2019/Docker.html
.. _GitLab Practicals: ../2019/Gitlab.html
.. _Kubernetes (Demo): ../2019/Kubernetes-Demo-2019.html
.. _Kubernetes Practical: ../2019/Minikube-and-NGINX-Practical-2019.html
.. _Advanced Kubernetes Practical: ../2019/Scaling-up-Kubernetes.html
.. _Advanced Kubernetes Reading: ../2019/Important-considerations-for-research-pipelines.html
.. _Deployment and Deployment Strategies: ../../_static/pdf/resops2021/advance_k8s_part_1.pdf
.. _Service Mesh: ../../_static/pdf/resops2021/advance_k8s_part_2.pdf

.. footer:: These resources are being developed by Cloud Consultants team of EMBL-EBI .

