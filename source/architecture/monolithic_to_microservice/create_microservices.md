## Creating Microservices

### Consideration 
  * Independent ability: Identify the components which are capable of running independently.
  * Sustainability: Strategy about the independent service code maintainability.
  * Infrastructure management: Consider the infrastructure and the how you are going to load balance
  * Resource management: Consider how many teams are going to develop the servers and number of members in one team.


### Segregation
Couple of approach to for segregation
  1. Tried segregating business logic from application
  2. Find isolated code, as they are already isolated. It will be fast to manage them.
  3. Application analysis for logic that might be helpful for configuration management, In short separate config related stuff and create configuration.

Following process may require to refactor your code and bring code management as first step for application to make decoupled. Recommend to perform refactoring into legacy code then test same into production before creating first separate service.

Few recommendation service which should be separate as first stage:
  * Application Authentication/Authorization Session management     
  * User/Account management as separate and application level too for managing user base functionality.
  * Configuration/Preference(Tools and default parameters) settings
     * Decoupled config management is insist as best practices.
  * Notification and communication services
  * Workflow services
     * Workflow configuration management
     * Job preparation service
  * Task queue/Job scheduler service
     * Queuing service
       * Job submission service
       * Job monitoring service     
  * Metadata/Data catalog service

### An application example

  ![alt text](../../static/images/Microservices_app_examples.png "example_microservices_app") 
  
### Design Changes
Application design suggested as per microservices below, **This is not a folder structure**
  
    Application           # Main application architecture, consist of below microservices.
      App.config          # Application's configuration management
        requirements      # contains all dependencies
        ..
      App.UI/             # UI architecture's JS framework (Node, Angular/React/Vue etc.)
        App_UI.config     # Must contain configuration separate
        requirements      # contains all dependencies
        ..
        ..
      App.API/            # API architecture's Web framework eg. Python/Java/Perl
        App_API.config    # Must contain config
        requirements      # contains all dependencies
        ..
        ..
      App.WF/             # Workflow, pipeline package
        App_WF.config     # Must contains configuration separate
        requirements      # contains all dependencies, like tool and version
        ..
        ..
      App.UM/             # Application's User Management
        App_UM.config     # Must contains configuration separate
        requirements      # contains all dependencies, like LDAP/OPENID UserDB
        ..
        ..
      App.BPM/            # Application's Business process management
        App_BPM.config    # Must contains configuration separate
        requirements      # contains all dependencies


### Strategy to create microservices architecture of existing application
  
  * __Bread slice butter approach__: In this approach application getting converted in microservice in smaller chunks. Get one component form the monolithic application and develop it as a microservice then put it into production. This is time taking strategy but safe and healthy. This strategy reduces the migration risk with the transition.
    
    * __Split Frontend and Backend__: split the presentation layer from the business logic and data access layers. A typical enterprise application consists of at least three different types of components: 
        * Presentation layer - Component that handle http/https requests, User Interface of Web technology, REST API layer.
        * Business layer - Core component of application, business rule. for bioinformatician this could be (data analysis pipeline, workflow, pipeline, job preparartion etc.)
        * Data-access layer - Accessing infrastructure like database, file system, messages, service access. 
      
    ![alt text](../../static/images/bread_slice.png "Bread slice butter")

    * __Module Extraction__: Similar to split frontend & backend; Bidirectional API need to be inplace, Business logic implemented using domain driven architecture or domain model pattern. Need to make significant code changes to break these dependencies. 
    
    ![alt text](../../static/images/extraction_approach.png "extraction module")   
  
  * __Lego block__: Not going to completely remove the monolithic application. Develop new features as microservices and keep the existing monolithic application as it is. This kind of architecture is called hybrid architecture. The plus point of this strategy is that it can reduce the workload and features can be developed within short time periods.
   Disadvantage is, it is required to maintain the legacy system as well as the microservices and system integration is very hard.  
   
      * __Glue Code (Stop making monolith bigger)__: Whenever new functionality require, shouldn't add more code to monolith code. Instead, the big idea with this strategy is to put that new code in a standalone microservice. an example below:
  
  ![alt text](../../static/images/glue_code_in_microservices.png "Glue_project")

  * __New Beginning or New Life Strategy__: The entire monolithic application is written using the microservices form the scratch. The main advantage of this strategy is that the organization can overcome all the bugs in the monolithic application, can choose whatever technology for the new building microservices and also can engage new features than the monolithic application. For this strategy, the organization needs to put more effort and more investments.

