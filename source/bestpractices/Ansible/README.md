# Ansible - IT Automation Engine
##  Configuration management and orchestration tool

## Ansible BP - Directory Layout

    dir_layout/
    ├── README.md
    ├── dbservers.yml
    ├── filter_plugins
    ├── group_vars
    │   ├── group1
    │   └── group2
    ├── host_vars
    │   ├── hostname1
    │   └── hostname2
    ├── library
    ├── production
    ├── roles
    │   ├── common
    │   │   ├── defaults
    │   │   │   └── main.yml
    │   │   ├── files
    │   │   │   ├── bar.txt
    │   │   │   └── foo.sh
    │   │   ├── handlers
    │   │   │   └── main.yml
    │   │   ├── meta
    │   │   │   └── main.yml
    │   │   ├── tasks
    │   │   │   └── main.yml
    │   │   ├── templates
    │   │   │   └── ntp.conf.j2
    │   │   └── vars
    │   │       └── main.yml
    │   ├── fooapp
    │   ├── monitoring
    │   └── webtier
    ├── site.yml
    ├── stage
    └── webservers.yml

