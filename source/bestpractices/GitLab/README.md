# Best Practices of GitLab

> **Use feature branches, no direct commit on master/qa until feature was not tested on dev branch.**

If you're coming over from SVN, for example, you'll be used to a trunk-based workflow. When using Git you should create a branch for whatever you’re working on, so that you end up doing a code review before you merge.
> **Test all commits, not only ones on master/qa.**

Some people set up their CI to only test what has been merged into **master**. This is too late; people should feel confident that **master** always has green tests. It doesn't make sense for people to have to test **master** before they start developing new features, for example. CI isn’t expensive, so it makes the best sense to do it this way.
> **Run all the tests on all commits (if your tests run longer than 5 minutes have them run in parallel).**

If you're working on a feature branch and you add new commits, run tests then and there. If the tests are taking a long time, try running them in parallel. Do this server-side in merge requests, running the complete test suite. If you have a test suite for development and another that you only run for new versions; it’s worthwhile to set up [parallel](https://docs.gitlab.com/ee/ci/yaml/README.html#stages) tests and run them all.
> **Perform code reviews before merges into master, not afterwards.**

Don't test everything at the end of your week. Do it on the spot, because you'll be more likely to catch things that could cause problems and others will also be working to come up with solutions.
> **Deployments are automatic, based on branches or tags.**

If you don't want to deploy **master** every time, you can create a **production branch**; but there’s no reason why you should use a script or log in somewhere to do it manually. Have everything automated, or a specific branch that triggers a [production deploy](https://docs.gitlab.com/ee/ci/yaml/README.html#environment).
> **Tags are set by the user, not by CI.**

A user sets a **tag** and, based on that, the CI will perform an action. You shouldn’t have the CI change the repository. If you need very detailed metrics, you should have a server report detailing new versions.

> **Releases are based on tags.**

If you tag something, that creates a new release.

> **Pushed commits are never rebased.**

If you push to a public branch you shouldn't rebase it since that makes it hard to follow what you're improving, what the test results were, and it breaks cherrypicking. We sometimes sin against this rule ourselves when we ask a contributor to squash and rebase at the end of a review process to make something easier to revert. But in general the guideline is: code should be clean, history should be realistic.
> **Everyone starts from master, and targets master.**

This means you don’t have any long branches. You check out **master**, build your feature, create your merge request, and target master again. You should do your complete review **before** you merge, and not have any intermediate stages.
> **Fix bugs in master first and release branches second.**

If you find a bug, the **worst** thing you can do is fix it in the just-released version, and not fix it in **master**. To avoid it, you always fix forward. Fix it in **master**, then **cherry-pick** it into another patch-release branch.

Read more at the [GitLab Flow documentation](https://docs.gitlab.com/ee/workflow/gitlab_flow.html)


## GitLab CI: Some best practices  

> **Read the docs.** read them again. [GitLab CI/CD](https://docs.gitlab.com/ce/ci/yaml/)

documentation of this is a great resource of setup.
> **Use GitLab CI Multi Runner.**

You can use fetch to make it faster, that’s ok. we recommend running your own CI runner VMs, with a shell runner inside, if you have resources for that.
> **Use stages.**

You want stages for many reasons. One is so you can see WHERE In the build it failed. Did it fail in compile_dependencies or did it fail in unit_tests or did it fail in integration_tests, or main_app_build, or did it fail at deploy?
> **Add tasks, and assign them stages. Set what they depend on.**

Things which can be done in parallel, make part of same stage and not depend on each other. Imagine you want to run “test_suite_a” and “test_suite_b” in parallel, make them separate tasks in the “testing” stage.
> **Use the “allow_failure” flag for stages that might fail for non-code-errors.**

Deployment can fail due to network issue or credentials or something else.
> **Must use Artifacting**

Artifacting to pass outputs of one stage to another. Also use artifacting to let you download each build as a zip, and maybe manually deploy it somewhere for testing, or just to “look at yesterday’s build outputs and compare them to todays”.
> **Use Caching**

use caching to speed up repeated builds on languages like C and C++ that generate a lot of .obj files and where their compilers are faster the second time, if you cache the intermediate files. Caching does not pass from one stage to another unless you define both caching and artifacting.
> **No stages which last >= 30min, Rule of thumb**

Don’t make stages that last more than 30 minutes.
> **Split massive tests into multiple tasks**

When you have a lot of tests, split your test task into multiple tasks and let them run in parallel.
> **Production deployment must be a manual task.**

maybe make production deployment a task, which is Manual? maybe make review/integrations deployment automatic, to a different server?
