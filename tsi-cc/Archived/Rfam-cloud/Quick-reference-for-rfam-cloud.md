## Quick reference for rfam cloud

Dear Tony and David,

It was very nice meeting you today and thank you very much for your time. 
I’m sending you the information as we discussed earlier. This includes our GitHub repository, physical paths on the cluster, my docker hub repo, some information regarding the process of building Rfam families and the various tools of the pipeline along with the exact numbers in terms of memory and cpus required to launch the searches. I also included Pablo Moreno’s kubespray GitHub repo and KubeNow, which I’m currently using to deploy and manage the K8s cluster on Embassy Cloud (testing only).

A. GitHub repository:
I need to bring to your attention the two branches that we are currently using:
1. master  - production code currently running on the cluster
2. rfam-cloud - updated version of the code with modifications to run within docker containers
master branch:
https://github.com/Rfam/rfam-family-pipeline

rfam-cloud branch:
https://github.com/Rfam/rfam-family-pipeline/tree/rfam-cloud


B. Cluster:
The following are the paths used by the production pipeline on the cluster
* Rfamseq location: /nfs/production/xfam/rfam/rfamseq/14
* Production pipeline: /nfs/production/xfam/rfam/rfam_rh7/production_software/rfam_production/rfam-family-pipeline/Rfam

C. Pre-built image on docker hub:
I also have a pre-built image available on docker hub with the test subset of our sequence database (Rfamseq) that I mentioned during our meeting. The dataset is available on the ftp, so if you want to build the image yourselves it will pull the data automatically from there. You can look into the configuration file rfam.conf (https://tinyurl.com/ycdrgtn3) for the exact locations where the different files reside within the container. Rfamseq is currently under directory /Rfam/rfamseq. The container also connects to our public MySQL database (https://rfam.readthedocs.io/en/latest/database.html), so once you get a running container it is ready for testing. 

This is the image on docker hub: 
ikalvari/rfam-cloud:rfam-fbp

Please note that any other images under that repository are only used for testing purposes and getting myself familiar with K8s. 


D. Using the pipeline to build families

For additional information on how our pipeline is being used to build families you can use the following:

A draft user’s guide on google docs -  https://tinyurl.com/y9eu7bnt
Documentation on Rfam help - https://rfam.readthedocs.io/en/latest/building-families.html

As I already mentioned, the most critical tool in the pipeline is rfsearch.pl (https://tinyurl.com/y7qywjw6). This is the one that uses LSF job arrays to launch 100 jobs (1 for each rfamseq chunk). The number is adjustable so we can change that accordingly. From my experience so far with kubernetes and my understanding of how it works, I’m getting the sense that we would only need to expand the functionality of rfsearch to use kubernetes kubectl (k8s cli) to run the searches using pods or jobs with some level of parallelization.


E. Additional information

Sequence database:
* 1 x Rfamseq size: 355 GB
* 100 x Rfamseq chunks (e.g. r100_rfamseq14_85.fa.gz): ~1GB
* 10 x Reversed Rfamseq chunks (e.g. rev-rfamseq14_1.fa.gz): ~1G

rfsearch requirements:
* 100 x jobs (one for each  r*_rfamseq14_85.fa.gz)
* 4 cpus/jobs by default
* 12GB memory - Memory requirements are calculated as: number of cpus * gigabytes per thread * 1000. The number of cpus and gigabytes per thread are configurable, but default to 4 and 3 respectively. So with the default parameters a job would require 4 cpus and 12GB of memory. 

Kubespray: 
https://github.com/pcm32/kubespray-ebi-portal

Kube Now:
https://kubenow.readthedocs.io/en/stable/getting_started/bootstrap.html

I hope this helps. Please feel free to look around and let me know if there’s anything that isn’t clear. I will be at your disposal in case you have any questions. 

Your help will be very much appreciated. Many thanks in advance.

Kind regards,
Ioanna

Ioanna Kalvari
Software Developer - Rfam
European Bioinformatics Institute (EMBL-EBI)

Tel:    + 44 (0) 1223 494279
Fax:    + 44 (0) 1223 494468
Email:ikalvari@ebi.ac.uk

