## Requirements and status 2018.10.09

Hi David,

That’s great! Thank you for looking into this and for scheduling our meeting so quickly. I’m happy to try out the k8s cluster you created on the Cloud portal. Please feel free to ping me on slack with further guidelines. 

As I said yesterday, I think at this point the Cloud portal isn’t going to be very useful as I already have a k8s cluster up and running. It could however make our collaboration easier in case you want us to use a common workspace by setting up team accounts. I’ll request for an additional floating ip in my Embassy tenancy so that I can start playing with it. 

If we design this properly and I think you will all agree, either we use the Cloud portal or an alternative way to spin up the k8s cluster shouldn’t be an issue. For example, an alternative could be Pablo Moreno’s kubespray-ebi-portal. Pablo is one of the developers of PhenoMeNal, one of the projects listed as funding resources of the Cloud portal. A GitHub repo to host all description yaml files we write for the cluster components should be enough to allow us to easily migrate this anywhere. 

I have a couple of milestones that I need help with:
1. Have a file system the cluster communicates with for R/W purposes. Using KubeNow this is being take care of with a glusterFS and heketi that manages the creation of PVs and PVCs associated with any pods we launch. Presumably where the sequence database will also be stored that will need to be accessible to all pods in the cluster and the master node for admin purposes. 
2. Have the output of the pods somehow replicated in the user home directories. We need persistent volumes because we want the state of the user account to be recoverable each time they log in. 
3. Modify the pipeline to use k8s components to process the data (PVs, PVCs, Pods, Jobs etc.. ). Not sure if it’s the right way to go, but I’ll probably launch user associated pods that get deleted after the process completes, to release resources. 
4. Load balancing - We will need a load balancer to efficiently distribute workload to the notes in the cluster. KubeNow and Kubespray work with Traefik at the moment.

1 and 2 are the most crucial for now as they will allow me to proceed with implementing 3 and test my work. If we have 1&2 addressed within the following two weeks, I can have 3 early November. 

I don’t have sys admin experience so feel free to correct me and provide better solutions. In case we go with the Cloud portal, we'll need options for 1&4 too. Does the k8s cluster running on the Cloud portal come pre-configured with those? And how easy is it to attach components to it? We also need to keep in mind integration with Galaxy soon after we have this running in production. 

I hope things are more clear now. Please feel free to contact me if you have any questions. 

Many thanks in advance.

Kind regards,
Ioanna

Ioanna Kalvari
Software Developer - Rfam
European Bioinformatics Institute (EMBL-EBI)

Tel:    + 44 (0) 1223 494279
Fax:    + 44 (0) 1223 494468
Email:ikalvari@ebi.ac.uk

