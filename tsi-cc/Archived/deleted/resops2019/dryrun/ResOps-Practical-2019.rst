ResOps Practical 2019
=====================

Demo: Creating VMs on Embassy
-----------------------------

* Log onto `Embassy for ResOps <https://extcloud05.ebi.ac.uk>`_. The tenancy is empty.
* Start IntelliJ and inspect what's in `resops.tf`, `terraform.sh` and under `kubespray/contrib/terraform/openstack`.
* `cd ~/IdeaProjects/tsi-ccdoc/tsi-cc/ResOps/` in the terminal window.
* Run `terraform.sh` one line at a time. Explain details.
* Log back onto Embassy to show VMs, network, subnet, router are created
* Access the VMs via SSH directly if they have public IPs attached. Otherwise, use SSH tunnel via bastion server, for example `ssh -i ~/.ssh/id_rsa -o ProxyCommand="ssh -W %h:%p -i ~/.ssh/id_rsa ubuntu@193.62.55.99" ubuntu@10.0.0.6`
* Add individual's public key to ~/.ssh/authorized_keys on the new VMs.

Exercise 1: Adding Terraform and Ansible to the new VMs
-------------------------------------------------------

Access the virtual workstation via bastion server, for example `ssh -i ~/.ssh/id_rsa -o ProxyCommand="ssh -W %h:%p -i ~/.ssh/id_rsa ubuntu@193.62.55.99" ubuntu@10.0.0.6`. Install Terraform and Ansible with the following commands::

  sudo snap install terraform
  sudo apt update && sudo apt install ansible -y
  sudo apt install python-pip -y
  sudo pip install yasha

TROUBLESHOOTING: If ~/.ssh/known_hosts contains an entry to of the virtual development workstation already. It may be necessary to delete it in order to exchange the key again. Note that `-o StrictHostKeyChecking=No` would not work.

Exercise 2: CPA Template
------------------------

See README for `CPA template <https://github.com/EMBL-EBI-TSI/cpa-template>`_ for details how to generate a CPA from the template::

  cd ~
  git clone https://github.com/EMBL-EBI-TSI/cpa-template
  cd cpa-template/
  ls -l ecp

The directory is empty. Generate a CPA from the template. It is Redis in this example::

  ./run.sh
  ls -l ecp

Explore the generated files and directories in the following exercises.

Exercise 3: Terraform scripts
-----------------------------

Open instance.tf by the following commands::

  cd ~/cpa-template/ecp/ostack/terraform
  nano instance.tf

Terraform is a declarative language. With the follow description, an OpenStack VM can be created on a network with security groups attached and a floating IP associated::

  resource "openstack_compute_instance_v2" "instance" {
    count       = "${var.instances}"
    flavor_name = "${var.machine_type}"
    key_pair    = "${var.name}-keypair"
    image_name  = "${var.disk_image_name}"
    name        = "${var.name}-${count.index + 1}"

    network {
      name           = "${var.network_name}"
      access_network = true
    }

    security_groups = [
      "${openstack_compute_secgroup_v2.allow-icmp.name}",
      "${openstack_compute_secgroup_v2.allow-ssh.name}",
      "${openstack_compute_secgroup_v2.allow-redis.name}",
    ]
  }

  resource "openstack_compute_floatingip_associate_v2" "instance_public_ip" {
    floating_ip = "${openstack_networking_floatingip_v2.floatingip.address}"
    instance_id = "${openstack_compute_instance_v2.instance.id}"
  }

Exercise 4: Ansible scripts
---------------------------

Open requirements.yaml with the following commands::

  cd ~/cpa-template/ecp/ostack/ansible
  nano requirements.yaml

Ansible roles can be downloaded as required in requirements.yml::

  - name: davidwittman.redis
    version: master
    src: davidwittman.redis

Then the role can be recalled in playbook.yml::

  - name: Set up redis_bind
    hosts: instance_public_ip
    become: yes
    vars:
      - redis_bind: "127.0.0.1"
      - redis_version: "{{lookup('env','TF_VAR_redis_version')}}"
      - redis_bind: "{{lookup('env','TF_VAR_redis_bind')}}"
    roles:
      - role: 'davidwittman.redis'

Exercise 5: Bash scripts
------------------------

The Terraform and Ansible scripts need to be stitched together by Bash scripts: deploy.sh, destroy, state.sh. Explore all three of them to understand how Terraform and Ansible work. The most important one is deploy.sh::

  cd ~/cpa-template/ecp/ostack
  nano deploy.sh

Pay close attention to see how `terraform apply`, `terraform output`, `ansible-galaxy install` and `ansible-playbook`::

  ###############
  # TERRAFORM
  ###############
  # Provision cloud resources
  echo -e "\n\t${CYAN}Terraform apply${NC}\n"
  terraform apply --state=${DPL}'terraform.tfstate' ${APP}'/ostack/terraform'

  # Extract runtime variables using terraform output
  external_ip=$(terraform output -state=${DPL}'terraform.tfstate' external_ip)
  ###############
  # ANSIBLE
  ###############
  # Install Ansible requirements with ansible galaxy
  echo -e "\n\t${CYAN}Install Ansible requirements with ansible galaxy${NC}\n"
  cd ostack/ansible || exit
  ansible-galaxy install --force -r requirements.yml

  # Set default value for Ansible variables if they are either empty or undefined
  ... ...

  # Launch Ansible playbook
  echo -e "\n\t${CYAN}Launch Ansible playbook${NC}\n"
  ansible-playbook -b playbook.yml

Exercise 6: manifest.json
-------------------------

The backend scripts needs to be connected with ECP GUI. Review `manifest.json` to see how it works::

  cd ~/cpa-template/ecp/
  nano manifest.json

The JSON describes the CPA so that the ECP frontend would know how to render itself, what to supply and what to receive::

  {
      "applicationName": "redis",
      "contactEmail": "gianni@ebi.ac.uk",
      "about": "Redis Server instance",
      "version": "0.1",
      "cloudProviders": [
          {
              "cloudProvider": "OSTACK",
              "path": "ostack"
          }
      ],
      "inputs": [
          "instance_number",
          "redis_bind"
      ],
      "deploymentParameters": [
          "redis_version",
          "parameter_2_name",
          "disk_image_name",
          "remote_user",
          "machine_type",
          "floating_ip_pool",
          "network_name"
      ],
      "outputs": [
          "external_ip"
      ]
  }

Exercise 7: Git repository for CPA
----------------------------------

* Create an empty project on `GitHub <https://github.com>`_, assuming that the name is `whatever`.
* For simplicity of the exercise, make sure that the project is public.
* git config --global user.name "User Name"
* git config --global user.email "username@ebi.ac.uk"
* git clone https://github.com/davidyuyuan/whatever.git
* cp -R ~/cpa-template/ecp/* whatever
* cd whatever. It should look like
* git add --all
* git commit -m "Initial check-in"
* git push -u origin master with ID and password or the personal access token. You can generate `the token <https://github.com/settings/tokens/new>`_ with 'repo' scope.

Exercise 8: Deployment and testing
----------------------------------

The first deployment would FAIL!!! We have embeded a bug in the template. This is to demonstrate a common mistake and investigation process. If you are running out of time, read through this exercise then follow the instructions in the next exercise to fix the bug before deploying your CPA.

* Add the CPA to `ECP repository <https://cloud-portal.ebi.ac.uk/repository/>`_ by clicking the '+' on the bottom-right, copy and paste the URL `https://github.com/davidyuyuan/whatever <https://github.com/davidyuyuan/whatever>`_ into the dialogbox.
* Click the repository `whatever` and select the configuration. ECP configuration is shared with you via a team.
* Jump to step 4 `Deploy` and click `Deploy` to start the deployment.

The deployment would fail with a mysterious error, for example::

  fatal: [193.62.55.59]: FAILED! => {"changed": false, "msg": "Unable to start service redis_6379: Job for redis_6379.service failed because a configured resource limit was exceeded. See \"systemctl status redis_6379.service\" and \"journalctl -xe\" for details.\n"}
  to retry, use: --limit @/mnt/ecp/data/be_applications_folder/usr-49663631-ba67-48cd-afdb-0e454aa4fb33/whatever/ostack/ansible/playbook.retry

  PLAY RECAP *********************************************************************
  193.62.55.59 : ok=24 changed=19 unreachable=0 failed=1

This error can be seen by expanding the log of the failed the deployment `https://cloud-portal.ebi.ac.uk/deployments/TSI1556031188648`. The deployment ID `TSI... ...` would be different every time. There are also other useful information on the details page. In particular, there is no error from Terraform. The problem is in the Ansible script. Thus, the investigation should focus on the Ansible script.

Dr. Google would not be able to tell you anything useful about this error. In addtion, you are unable to SSH to the VM to try anything manually at this point. Your public key has not been added to the server. If you try to deploy the CPA onto a larger VM, the deployment would fail with the same error message. The error message is very misleading. There is no resource limitation.

Exercise 9: Fixing a defect
---------------------------

A hard way, and usually an effective way, is to review the code or the recent changes to look for clues. Note that cpa-template is for generating project skeleton. You should start making changes in the project `whatever` directly from now on.

The manifest.json is the single most important file in a CPA project. Always pay particular attention to the file. You would notice a strange deployment parameter `parameter_2_name` makes no sense. All the other parameters have meaningful names.

In addition, most of the other parameters are the usual input variables for OpenStack and one for Redis. Is it possbile that an input for Redis was not fully implemented?

At the deployment stage, `ostack/deploy.sh` is the entry point, thus should be the very next one to be reviewed. This file itself looks bug-free. The last line `ansible-playbook -b playbook.yml` leads you to the entry point of the Ansible script `playbook.yml`.

You are likely notice something strange very quickly in the `ostack/ansible/playbook.yml`. An variable `redis_bind` is defined twice::

  - name: Set up redis_bind
    hosts: instance_public_ip
    become: yes
    vars:
      - redis_bind: "127.0.0.1"
      - redis_version: "{{lookup('env','TF_VAR_redis_version')}}"
      - redis_bind: "{{lookup('env','TF_VAR_redis_bind')}}"
    roles:
      - role: 'davidwittman.redis'

First, redis_bind is hardcoded to the loopback adaptor `127.0.0.1`. It is overridden by an environment variable `TF_VAR_redis_bind`.

Tracing back to `ostack/deploy.sh`, the Bash script has done the best to ensure that `TF_VAR_redis_bind` is a required parameter by setting its defalt value to `None` right before the Ansible role is called::

  export TF_VAR_redis_bind="${TF_VAR_redis_bind:-None}"
  echo "export TF_VAR_redis_bind=${TF_VAR_redis_bind}"
  export ANSIBLE_REMOTE_USER="${TF_VAR_remote_user:-centos}"
  echo "export ANSIBLE_REMOTE_USER=${ANSIBLE_REMOTE_USER}"

  # Launch Ansible playbook
  echo -e "\n\t${CYAN}Launch Ansible playbook${NC}\n"
  ansible-playbook -b playbook.yml

That's why you see the following in the log::

  export TF_VAR_redis_bind=None
  export ANSIBLE_REMOTE_USER=centos
  Launch Ansible playbook

Tracing further back to `manifest.json`. The deployment parameter `redis_bind` is not defined. Input `redis_bind` is optional thus can easily be missed by users.

To clean this up, modify four files: `manifest.json`, `deploy.sh`, `playbook.yml` and `README.md`.

* In `manifest.json`, change `parameter_2_name` to `redis_bind`.
* In `deploy.sh`, change `export TF_VAR_redis_bind="${TF_VAR_redis_bind:-None}"` to `export TF_VAR_redis_bind="${TF_VAR_redis_bind:-127.0.0.1}"`.
* In `playbook.yml`, delete `- redis_bind: "127.0.0.1"`.

Make sure that the documentation is updated accordingly in `README.md`.

.. image:: /static/images/redis_bind.png

With this fix, `redis_bind` is no longer hard-coded in Ansible playbook. It now can be set as a deployment paramter, user input and has a default value of `127.0.0.1`, the loopback adaptor.

* To test the change, commit and push the modified files as you did in Exercise 7. It is still a good habit to review the changes in Git UI if you do want to be bothered with `git pull` on a one-man project.
* Click "Destroy" button on the bottom-left of the details page to delete the failed deployment.
* Click "delete" icon on the top-right of the repository wizard page to delete the repository.
* Repeat the steps in Exercise 8 to deploy again. The deployment should be successful this time.
* SSH to the VM via floating IP or SSH tunnel. The IP can be found on the deployment details page.
* Launch `redis-cli` to see the in-memory data store in action.

You would notice several differences.

* If you expand "Deployment parameters" on the deployment wizard, you will notice that `redis_bind` is now a parameter, instead of `parameter_2_name` name before. As it is not included in your profile, it is shown as `Not assigned`. You can update your profile to set a value there but it is unnecessary. Recall that the default is defined in `deploy.sh`.
* You may also notice that `redis_version` and `remote_user` have default values defined in `deploy.sh` just like `redis_bind`. It is OK to leave them `Not assigned` in `Deployment parameters`. All the other parameters such as `floating_ip_pool` take the values from your profile.
* Reviewing the log message on the deployment details page, you would find the binding is no longer `None`. The value would be whatever from your profile, GUI input or the default value `127.0.0.1` defined in `deploy.sh`.

Here is what the log message may be if no value is provided from your profile or GUI input::

  export TF_VAR_redis_version=3.0.7
  export TF_VAR_redis_bind=127.0.0.1
  export ANSIBLE_REMOTE_USER=centos
  Launch Ansible playbook

Side note: you may find that the other input value `instance_number` is only implemented at the frontend. This means that you can put any number you want in the browser but the value is not used by the backend. You can only create one instance regardless. There is no reference to it in `deploy.sh` or Terraform script or Ansible script. Imagine that this is an implementation half done by your teammate. You would need to develop the backend logic in Terraform and Ansible to make the input `instance_number` taking effect.

Demo: CI/CD pipeline for Rfam pipeline on K8S cluster
-----------------------------------------------------

* Start `CI/CD dashboard <https://gitlab.ebi.ac.uk/davidyuan/cicd-dashboard/pipelines>`_, which is a pipeline itself.
* Click `Run Pipeline` > `Create pipeline`.
* The dashboard starts and gets ready to create other pipelines to verify CPAs:

  * cpa-instance
  * cpa-kubernetes
  * cpa-nfs-server-and-vol
  * rfam-cloud

* Click a job, for example `rfam-cloud` to start a new pipeline to run Rfam on K8S cluster on OSK deployed through ECP.
* Go to the `CI/CD page in rfam-cloud project <https://gitlab.ebi.ac.uk/davidyuan/rfam-cloud/pipelines>`_ to see a pipeline is created and started running. The pipeline does the following:

  * Clone ECP CLI into a new Python virtual environment.
  * Create cloud credential, deployment parameter and ECP configuration for two CPAs in parallel on the ECP server.
  * Import the two CPAs into ECP server:

    * The `cpa-kubernetes <https://github.com/EMBL-EBI-TSI/cpa-kubernetes>`_ can create 3 VMs by default to run one master and multiple worker nodes.
    * The `cpa-nfs-server-and-vol <https://github.com/EMBL-EBI-TSI/cpa-nfs-server-and-vol>`_ can create 1 VM with one storage volume attached to run NFS server.

  * Deploy the two CPAs in parallel on Embassy.
  * SSH to the NFS server on Embassy to verify that the correct volume is mounted.
  * SSH to verify that the K8S cluster is running. In addition, the following actions are taken to make use of the new K8S cluster:

    * Create PV and PVC to make use of the NFS volume.
    * Pull an Rfam container from the Docker Hub.
    * Create a ReplicaSet of two or more pods of Rfam pipelines.
    * Run a simple regression to verify that the Rfam container is working.

Bonus: ECP for development
--------------------------

ECP GUI is used most of the time. ECP commandline interface (aka Python SDK) can be usedful for scripting auch as deployment automation or CI/CD testing. Install CLI with a simple `git clone`::

  cd ~
  git clone https://github.com/EMBL-EBI-TSI/ecp-cli
  alias ecp="python3 ~/ecp-cli/ecp.py"

log in to the cloud portal by running `ecp login` command. Follow the instructions to get an authentication token from a web browser. Make sure the token is quoted when providing it to the CLI::

  C02XD1G9JGH7:bin davidyuan$ ecp login
  Please visit https://api.aai.ebi.ac.uk/sso and follow the login instructions
  Please enter the token received here: "eyJhbGciOiJSUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FhaS5lYmkuYWMudWsvc3AiLCJqdGkiOiJqOFVSc2tpMmprcFM1ZXpmaVAxSHJBIiwiaWF0IjoxNTU1MTcxODA0LCJzdWIiOiJ1c3ItNDk2NjM2MzEtYmE2Ny00OGNkLWFmZGItMGU0NTRhYTRmYjMzIiwiZW1haWwiOiJkYXZpZHl1YW5AZWJpLmFjLnVrIiwibmlja25hbWUiOiJmOTQ3YTMyMmM3MDcwM2FhNjZlZjQzODQ4YTNmMTc3NGFlY2M1MGYzIiwibmFtZSI6IkRhdmlkIFl1YW4iLCJkb21haW5zIjpbInNlbGYuVEVBTV9FTUJMLUVCSV9QT1JUQUxfVVNSLTM2RDBGMUM2LTUyN0QtNDQwOC1BRTZELTdDNDI1QTAyMjA4NyIsInNlbGYuVEVBTV9FTUJMLUVCSS1ERVZfUE9SVEFMX1VTUi0zNkQwRjFDNi01MjdELTQ0MDgtQUU2RC03QzQyNUEwMjIwODciXSwiZXhwIjoxNTU1MTc1NDA0fQ.Nx5RmOfvjcFpr4_gQtQrB0KHhHdnCRIFiFqjrAAWFQeYE8s45Ikrw3ZBl08fplLQ4Y-aXIq8QDzmp8CEBnWKvdKJRphw_OVDclypKmztTjnhqBiYDPogmiElp7BeIbG0emXtJY5f81HIceZwAipdJu-LLPMKDglsSw9tqYeQ4lsaixWL6-_ulqeq_i2go5tUoU6J16dnity315WhcF0jItgdB7iMxHgZZbEr09-28U4ov1gcShKAyZxWK34ttIjT3T3-l4Sv5TYoMSTBayZj2nU3H00-CfUKAN2zK_bviJ6y8tFzcH9gsLsYNdBUJhSDMWiKF18G7ZnvHpcJJUSTSQ"
  Login successful!

Nice try. Cloud redentials are encrypted in ECP. Not even the own can see the plaintext values again::

  C02XD1G9JGH7:bin davidyuan$ ecp get cred
  - davidyuan-extcloud05:
      Provider: OSTACK
      Parameters:
      * OS_REGION_NAME: ENCRYPTED VALUE
      * OS_PASSWORD: ENCRYPTED VALUE
      * OS_AUTH_URL: ENCRYPTED VALUE
      * OS_USERNAME: ENCRYPTED VALUE
      * OS_TENANT_ID: ENCRYPTED VALUE
