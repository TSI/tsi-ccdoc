Scaling up Kubernetes for G1K variant calling
=============================================

For research pipelines, there are some major considerations when moving to the clouds:

* Accessing large amount of data from the source

  * `ReadOnlyMany for data source`_
  * `ReadWriteOnce for private workspace`_
  * `ReadWriteMany for shared output`_
  * `Initialising persistent volumes`_

* `S3 interface`_
* `Workflow management`_
* `Resource consumption`_
* `Docker builds`_
* `CI/CD toolchain`_
* `Bringing compute close to data`_

Here is a pipeline for variant calling on Google GKE. The workflow engine Nextflow and Kubernetes are to be integrated to scale the pipelines and to schedule jobs. The containers such as Samtools and Freebayes read and write on persistent volumes serving as database and S3 buckets.

.. image:: /static/images/resops2019/NextflowVCF.png

ReadOnlyMany for data source
----------------------------

For pipelines running in Embassy cloud, there are several ways to access the shared storage directly. This can avoid copying large amount of data.

* For VMs in the Embassy cloud, open a `ticket with VAC team <mailto:vac@ebi.ac.uk>`_ for network setup then configure your VMs following their article `Connecting to our shared storage <https://www.ebi.ac.uk/seqdb/confluence/display/EMBCLOUD/Connecting+to+our+shared+storage>`_.
* For Kubernetes clusters in the Embassy cloud, open a `ticket with VAC team <mailto:vac@ebi.ac.uk>`_. They can set up the network and configure the worker nodes. You would need to define PersistentVolume and PersistentVolumeClaim for Kubernetes to bind and mount the storage.

Here is a sample of PV and PVC::

  apiVersion: v1
  kind: PersistentVolume
  metadata:
    name: pv1000g
  spec:
    capacity:
      storage: 100Ti
    accessModes:
      - ReadOnlyMany
    nfs:
      server: "<host name or IP>"
      path: "<mount path>"
  ---
  apiVersion: v1
  kind: PersistentVolumeClaim
  metadata:
    name: pv1000g
  spec:
    accessModes:
      - ReadOnlyMany
    storageClassName: ""
    resources:
      requests:
        storage: 100Ti

Note that Kubernetes has a `detailed list <https://kubernetes.io/docs/concepts/storage/persistent-volumes/>`_ of kinds of volumes and access modes supported. Use it creatively, you should be able to avoid copying data within EBI. NFS is necessary evil. Use it only if it is unavoidable.

The read-only volume bound in the beginning can be mounted by containers in a pod. There are two steps.

In the pod template, refer to the PersistentVolumeClaim pv1000g::

      volumes:
        - name: pv1000g
          persistentVolumeClaim:
            claimName: pv1000g
            readOnly: true

In both containers, defines the logical mount point that everything running in them would see::

          volumeMounts:
            - name: pv1000g
              mountPath: "/datasource/"

The tools, Samtools and Freebayes, running in their containers can access the human reference genome and assemblies from the 1000 Genome Project as if local files.

ReadWriteOnce for private workspace
-----------------------------------

The Samtools and Freebayes need to access the same workspace from two different containers. Samtools generate intermediate files (i.e. reference human genomes by chromosome). Freebayes performs variant calling on an assembled sequence by chromosomes. PersistenceVolumeClaim in StatefulSet provides a nice mechanism to address such requirement::

  volumeClaimTemplates:
    - metadata:
        name: private
      spec:
        accessModes:
          - ReadWriteOnce
        resources:
          requests:
            storage: 10Gi
        # Read more https://kubernetes.io/docs/concepts/storage/persistent-volumes/#class-1
        storageClassName: nfs-client

In both containers, defines the logical mount point making use of the claim. Both containers in the same pod mount to the same volume. Each pod has its own private volume::

          volumeMounts:
            - name: private
              mountPath: "/private/"

ReadWriteMany for shared output
-------------------------------

The output should be sent out directly from the pipeline if possible. However, most of the pipelines assume local POSIX file systems for output. It is necessary to define a shared storage for multiple pods to send output to::

  kind: PersistentVolumeClaim
  apiVersion: v1
  metadata:
    name: shared-workspace
  spec:
    storageClassName: nfs-client
    accessModes:
      - ReadWriteMany
    resources:
      requests:
        storage: 50Gi

In the pod template, refer to this shared volume claim::

      volumes:
        - name: shared-workspace
          persistentVolumeClaim:
            claimName: shared-workspace

In the container running Freebayes, define the mount point to be used for output::

          volumeMounts:
            - name: shared-workspace
              mountPath: "/workspace/"

Initialising persistent volumes
-------------------------------

There is no life-cycle management for persistent volumes in Kubernetes. The closest thing is command array in initContainers. Here is an example to create a subdirectory on a mounted volume `/workspace/`::

      initContainers:
        - name: init
          image: busybox
          command: ["/bin/sh"]
          args: ["-c", "mkdir -p /workspace/result/"]
          volumeMounts:
            - name: shared-workspace
              mountPath: "/workspace/"

Be careful when using multiple initContainers instead of one to configure the pod. The behaviour can be puzzling.

S3 interface
------------

To integrate Freebayes with other pipelines requiring S3 bucket, or to review output easily via a browser, Minio can be mounted to the storage for shared output. The manifest in the pod template and the container as the same as in `ReadWriteMany for shared output`_.

Make sure that the arguments to initialize the container must refer to the same mount point::

      containers:
        - name: minio
          image: minio/minio
          args:
            - server
            - /workspace/

Then, the subdirectories will be treated as S3 buckets by Minio.

Workflow management
-------------------

Kubernetes is an orchestration engine. It is understandable that it provides limited capability for workflow management. Kubernetes and some simple shell-scripting can scale pods and schedule jobs to be run in parallel::

  # Scale up to the number of pods desired
  sfsname=$(kubectl get statefulset -o name | grep -m 1 freebayes)
  commonname=$(echo ${sfsname} | cut -d '/' -f2)
  kubectl scale statefulsets ${commonname} --replicas=${num_of_pods}
  kubectl rollout status ${sfsname} --request-timeout=6m

In Bash script, you can then send jobs into each container in round-robin. This is the most simple-minded job scheduling. This shotgun approach can overwhelm pods easily. Jobs scheduled can fail due to lack of resources or timeouts. It is work-in-progress to integrate Kubenetes cluster with a workflow engine such as Nextflow.

Resource consumption
--------------------

It is of paramount importance to monitor resource consumption to understand where the bottlenecks are. Prometheus & Grafana provide a simple monitoring tool but useful enough to view resource dynamics. The screenshot below shows an attempt to scale up Kubernetes pods to call variants in all 1000 genomes.

* CPU was maxed out almost immediately.
* Memory consumption kept increasing over about a hour and a half.
* Disk IO, network IO, disk space and Inode usage were all OK.
* The system failed after runing for about 8 hours.
* The dashboards for other worker nodes showed the same behaviour.

There were seemingly random SIGABRT errors on the SSH client after a while.
There may not be enough CPU to handle scheduled jobs. There may be severe memory leaks by Freebayes, which eventually chocked up the pipeline. We can take the following actions:

#. Reducing the batch size so that there are less jobs waiting for CPU cycles
#. Adding more CPUs to the existing nodes and / or more nodes with larger number of CPUs
#. Using more sophisticated job scheduler instead of Bash script + Kubernetes

We would not know what to do otherwise. A resource monitor is useful for development, test and production.

.. image:: /static/images/resops2019/Prometheus1000g.png

Docker builds
-------------

There are two tools in the pipeline: Samtools and Freebayes. we have containerised the tools and wrapper / driver scripts. Here are some best practices to follow:

* Creating two separate containers to manage them separately at runtime.
* Starting with minimal base image.
* Creating CI/CD toolchain to buiid Docker images to avoid doing repetitive work.
* Downloading binaries from trusted source only.
* Building from the source if only necessary.
* Including wrapper / driver scripts in the container for good encapsulation.

Here are two examples of Docker files:

* `https://gitlab.ebi.ac.uk/davidyuan/nextflow/blob/master/gcp/freebayes/Dockerfile <https://gitlab.ebi.ac.uk/davidyuan/nextflow/blob/master/gcp/freebayes/Dockerfile>`_
* `https://gitlab.ebi.ac.uk/davidyuan/nextflow/tree/master/gcp/samtools/Dockerfile <https://gitlab.ebi.ac.uk/davidyuan/nextflow/tree/master/gcp/samtools/Dockerfile>`_

The build jobs can be part of a larger toolchain to create Kubernetes cluster, to deploy and to run workload. This gives you better control and nicer integration. Docker hub can build images when integrated with a source control repository directly. But it provdes limited resources with less control. There is private repository for Docker images at EBI. My personal preference is to push the image built on GitLab to Docker Hub. The build process can take the following steps::

    - git clone ${CI_GIT_URL}
    - cd ${CI_BUILD_CONTEXT}
    - docker login -u "${CI_REGISTRY_USER}" -p "${CI_REGISTRY_PASSWORD}" ${CI_REGISTRY}
    - docker build -f ${CI_DOCKER_FILE} -t ${CI_SOURCE_IMAGE} ${CI_BUILD_CONTEXT} | tee ${ARTIFACT_DIR}/build.log
    - docker tag ${CI_SOURCE_IMAGE} ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}
    - docker push ${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG} | tee ${ARTIFACT_DIR}/push.log
    - docker logout

You can simply perform the seven steps to build an image on a standard GitLab runner with DIND enabled. Here is a link to the images created with this process `https://hub.docker.com/?namespace=davidyuyuan <https://hub.docker.com/?namespace=davidyuyuan>`_.

CI/CD toolchain
---------------

In addition to creating Docker images, many other jobs are needed in order to assemble the pipeline to produce VCFs for 1000g. Here are the major tasks:

#. Building Docker images for target clouds
#. Creating / configuring Kubernetes cluster on Embassy (EHK) or Google (GKE)
#. Deploying pipeline into the environments and performing smoke-test
#. Clean up Kubernetes clusters when finished

The CI/CD toolchain below automates all the jobs. It improves the DevOps efficiency significantly. Here are the links to the CI files:

* `https://gitlab.ebi.ac.uk/davidyuan/nextflow/blob/master/.gitlab-ci.yml <https://gitlab.ebi.ac.uk/davidyuan/nextflow/blob/master/.gitlab-ci.yml>`_
* `https://gitlab.ebi.ac.uk/davidyuan/nextflow/blob/master/gcp/.gitlab-ci.yml <https://gitlab.ebi.ac.uk/davidyuan/nextflow/blob/master/gcp/.gitlab-ci.yml>`_
* `https://gitlab.ebi.ac.uk/davidyuan/nextflow/blob/master/osk/.gitlab-ci.yml <https://gitlab.ebi.ac.uk/davidyuan/nextflow/blob/master/osk/.gitlab-ci.yml>`_

.. image:: /static/images/resops2019/CICDforNextflow.png

Bringing compute close to data
------------------------------

In the era of cloud computing, compute is not the center of the universe. Data is. Always try to bring compute to data, instead of data to compute for big data problems. The key is to make the compute highly portable.

In this project, we have to investigate the possibility to run Freebayes / Samtools on GKE due to the computing resource allocated to worker nodes. This means that we no longer have "local" access to the data storage within EBI. There are many solutions to access data remotely, which also means that there is no single solution solving most of the problems without creating their own share of issues.

We are using a combination of `wget`, `gzip` and `samtools`. Different tools and strategies may be necessary to reduce the overall cost of network, cpu, memory, storage, and computing and development time.

