#!/usr/bin/env bash

accesskey=$1
secretkey=$2

kubectl apply -f source/static/scripts/minio/pvc.yml
kubectl get pvc
kubectl get pv

kubectl create secret generic minio --from-literal=accesskey=${accesskey} --from-literal=secretkey=${secretkey}
kubectl get secret

kubectl apply -f source/static/scripts/minio/minio.yml
kubectl get svc
# https://helm.min.io/
# helm repo add minio https://helm.min.io
# helm install minio --set replicas=1,service.type=NodePort,service.nodePort=32001,persistence.existingClaim=minio-pvc-nginx,existingSecret=minio,mountPath="/usr/share/nginx/" minio/minio
